import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Font } from 'expo';
import { NativeRouter, Route } from 'react-router-native';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }
  async componentDidMount() {
    await Font.loadAsync({
      'eczar-bold': require('./fonts/Eczar/Eczar-Bold.ttf'),

    });
    this.setState({
      loading: false
    });
  }

  render() {
    if (this.state.loading) {
      return (
        <View style={styles.container}></View>
      )
    }
    return (
      <View style={styles.container}>
        <NativeRouter>
          <Route exact path="/" render={styles => (<View style={styles.container}>
            <Text style={styles.text}>Create React Native App is awesome</Text>
            <Text style={styles.text}>Love reloading yujuuuu ❤️</Text>
          </View>)} />
        </NativeRouter>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: '#f5f5f5',
    fontFamily: 'eczar-bold'
  }
});
